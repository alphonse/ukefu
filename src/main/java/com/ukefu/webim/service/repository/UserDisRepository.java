package com.ukefu.webim.service.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.ukefu.webim.web.model.DisUser;

public abstract interface UserDisRepository extends JpaRepository<DisUser, String> {
	public abstract Page<DisUser> findByOrganAndDatastatusAndOrgiAndMobileNotNull(String paramString, boolean datastatus, String orgi , Pageable page);

}
