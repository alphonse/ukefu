package com.ukefu.webim.service.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ukefu.webim.web.model.SystemConfig;

public abstract interface SystemConfigRepository  extends JpaRepository<SystemConfig, String>
{
	public abstract List<SystemConfig> findByOrgi(String orgi);
}

