package com.ukefu.webim.service.es;

import java.util.List;

import org.elasticsearch.index.query.BoolQueryBuilder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.ukefu.webim.web.model.VoiceTranscription;

public abstract interface VoiceTranscriptionESRepository

{
  public abstract VoiceTranscription findByIdAndOrgi(String paramString, String orgi);
  
  public abstract List<VoiceTranscription> findByCallidAndOrgi(String callid, String orgi);
  
  public abstract Page<VoiceTranscription> findByOrgi(BoolQueryBuilder boolQueryBuilder ,Pageable page);
  
}
