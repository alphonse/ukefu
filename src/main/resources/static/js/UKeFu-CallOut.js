/**
 *权限数据采集 
 */
$(document).ready(function(){
	$('.ukefu-phone-number').addClass("ukefu-phone-class");
	$('.ukefu-phone-number').on("click" , function(){
		var type = $(this).data("type");
		var id = $(this).data("id");
		var phonenumber = $(this).text().trim();
		if(phonenumber != ''){
			top.layer.confirm('是否拨打'+ (type == "contacts" ? '联系人' : '') +'号码 ‘'+phonenumber+'’', {icon: 3, title:'拨打号码'}, function(index){
				top.layer.close(index);
				if(type == "contacts"){
					top.uKeFuSoftPhone.businvite(phonenumber , type , id);
				}else{
					top.uKeFuSoftPhone.invite(phonenumber);
				}
			});
		}
	});
});